use serde::Serialize;

#[derive(Clone, Debug, Serialize)]
struct MyObject {
    n: u64,
    s: String,
}

fn main() {
    println!("# common-patterns-rust\n");

    println!("* Creating an object");
    let obj = MyObject {
        n: 3,
        s: String::from("foo"),
    };
    println!("* Cloning it");
    let obj2 = obj.clone();

    println!("* Printing its contents for debugging");
    println!("{:?}", obj);

    println!("* Turning it into JSON");
    println!("{}", serde_json::to_string(&obj2).unwrap());
}
