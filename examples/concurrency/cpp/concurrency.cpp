#include <iostream>
#include <thread>
#include <vector>

using namespace std;

int main()
{
    cout << "# concurrency-rust-mutex\n\n";

    cout << "* Launching worker threads\n";
    vector<thread> threads;
    int counter = 0;
    for (int i = 0; i < 10000; ++i)
    {
        threads.push_back(thread([&counter]{++counter;}));
    }

    cout << "* Waiting for threads to finish\n";
    for (thread& t: threads)
    {
        t.join();
    }

    cout << "Total (oops, some may be missed!): " << counter << "\n";
}
