use std::sync::mpsc;
use std::thread;

fn main() {
    println!("# concurrency-rust-channels\n");

    println!("* Creating a communication channel");
    let (sender, receiver) = mpsc::channel();

    println!("* Launching worker threads");
    let mut worker_threads = Vec::new();
    for _i in 0..10000 {
        let s = sender.clone();
        worker_threads.push(thread::spawn(move || s.send(true).unwrap()));
    }

    println!("* Launching co-ordination thread");
    let counting_thread = thread::spawn(move || {
        let mut counter = 0;
        while receiver.recv().unwrap() {
            counter += 1;
        }
        counter
    });

    println!("* Waiting for workers to finish");
    for thread in worker_threads {
        thread.join().expect("Join failed");
    }

    println!("* Telling co-ordination thread to finish");
    sender.send(false).unwrap();
    let total = counting_thread.join().expect("Total join failed.");

    println!("Total: {}", total);
}
