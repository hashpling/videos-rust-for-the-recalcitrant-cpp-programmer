#include <iostream>
#include <vector>

using namespace std;

struct MyObject {
    int x;
};

int main() {
    cout << "# ownership\n\n";

    cout << "* Creating an object\n";
    MyObject obj{3};

    cout << "* Moving it into a vector\n";
    vector<MyObject> vec;
    vec.push_back(std::move(obj));

    cout << "* Oops... using it after move!\n";
    cout << obj.x << "\n";
}
