struct MyChild {
    age: u64,
}

impl Child for MyChild {
    fn set_age(&mut self, age: u64) {
        self.age = age;
    }

    fn get_age(&self) -> u64 {
        self.age
    }
}

trait Child {
    fn set_age(&mut self, age: u64);
    fn get_age(&self) -> u64;
}

struct Parent {
    child: Box<dyn Child>,
}

fn main() {
    println!("# immutability-cpp\n");

    println!("* Creating a const Parent+MyChild");
    let parent = Parent {
        child: Box::new(MyChild { age: 1 }),
    };

    println!("* Modifying it!");
    // ERROR: cannot borrow as mutable
    parent.child.set_age(10);
}
